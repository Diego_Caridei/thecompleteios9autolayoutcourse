# The Complete iOS9 Auto Layout Course
The goal is to raise your AutoLayout confidence levels and design iOS interfaces that use space effectively

#Description
In this course, my friend Ravi and I would like to raise your confidence levels, in that confounding but very important topic : iOS Auto Layout.

Building an App involves 2 key components - Writing Code and designing the user interface

When the app lands up with your audience, its interface design, plays a key role, in its success.

So a good grasp of AutoLayout will certainly help you design, better, user interfaces

Interfaces that deal ,with different screen sizes and orientations.

Interfaces that adapt to available space, smartly.

And interfaces, that are quick to build, and easy to maintain.

We Aim to show you all the key elements of Auto-Layout and you need no programming experience, since we will start from scratch

We will begin with an introduction to Xcode and focus, on key elements that come into play when designing the look and feel of an App

Then with the help of multiple case studies we will build, real world interfaces, and in the process, understand, the role various Xcode elements play.

Each lesson is complemented with a word cloud of key terms along with concept reinforcing quizzes and reference material

At the end of this course you will walk away with much, more confidence.

And lastly, we will continue to keep adding to the course material so that, its always up to date for you, our learner.

So, in conclusion, Ravi and I are proud to present this iOS Auto Layout course to you. We hope you will join us on this exciting journey and we look forward to chatting with you in the forum.

link: https://www.udemy.com/the-complete-ios9-auto-layout-course/learn/v4/overview

