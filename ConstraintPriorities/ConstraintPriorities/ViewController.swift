//
//  ViewController.swift
//  ConstraintPriorities
//
//  Created by Diego Caridei on 24/04/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var middleView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //flagUKraine()
       
    }

    
    func flagUKraine(){
        topView.backgroundColor = UIColor(red: 0, green: 0, blue:1 , alpha: 1)
        middleView.removeFromSuperview()
    }


}

