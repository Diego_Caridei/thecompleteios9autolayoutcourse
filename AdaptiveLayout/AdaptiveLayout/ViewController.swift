//
//  ViewController.swift
//  AdaptiveLayout
//
//  Created by Diego Caridei on 24/04/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func tapped(sender: AnyObject) {
        if sender.titleForState(.Normal)=="downfall"{
            sender.setTitle("Gotterdammerung", forState: .Normal)
        }else{
            sender.setTitle("Downfall", forState: .Normal)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

